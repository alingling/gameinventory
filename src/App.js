import { useState } from 'react'
import Header from './components/Header'
import Game from './components/Games'
import AddGame from './components/AddGame'

const App = () => {
  const [showAddGame, setShowAddGame] = useState(false)
  const [games, setGames] = useState([
    {
      id: 1,
      game_name: 'Chess',
      no_of_players: 2,
      playing_time: '2 hours',
      no_of_copies: 2
    },

    {
      id: 2,
      game_name: 'Snake and Ladder',
      no_of_players: 2,
      playing_time: '1 hour',
      no_of_copies: 2
    },

    {
      id: 3,
      game_name: 'Monopoly',
      no_of_players: 4,
      playing_time: '3 hours',
      no_of_copies: 4
    }


  ])

  //Add Game
  const addGame = (game) => {
    const id = Math.floor(Math.random() * 10000) + 1
    const newGame = { id, ...game }
    setGames([...games, newGame])
  }

  //DELETE GAME
  const deleteGame = (id) => {
    setGames(games.filter((games) => games.id !== id))
  }

  return (
    <div className='container'>
      <Header onAdd={() =>
        setShowAddGame(!showAddGame)}
        showAdd={showAddGame}
      />
      {showAddGame && <AddGame onAdd={addGame} /> }
      {games.length > 0 ? <Game games={games} onDelete={deleteGame} /> : 'No Games'}
    </div>
  );
}


export default App;
