import {useState} from 'react'

const AddGame = ({onAdd}) => {
    const [game_name,setText] = useState('')
    const [no_of_players,setPlayer] = useState('')
    const [playing_time,setPlayingTime] = useState('')
    const [no_of_copies,setCopiesOwned] = useState('')

    const onSubmit = (e) => {
        e.preventDefault()

        if(!game_name){
            alert('Please add a game')
            return
        }

        onAdd({game_name,no_of_players,playing_time,no_of_copies})

        setText('')
        setPlayer ('')
        setPlayingTime('')
        setCopiesOwned('')
    }
    return (
        <form className='add-form' onSubmit={onSubmit}>
           <div className='form-control'>
                <label>Board Game</label>
                <input type='text' placeholder='Add Board Game Name' value={game_name} onChange={(e) => setText(e.target.value)} />
            </div>
            <div className='form-control'>
                <label>No of Players</label>
                <input type='number' min='1' value={no_of_players} onChange={(e) => setPlayer(e.target.value)}/>
            </div>
            <div className='form-control'>
                <label>Required Playing Time</label>
                <input type='text' placeholder='' value={playing_time} onChange={(e) => setPlayingTime(e.target.value)} />
            </div>
            <div className='form-control'>
                <label>No of Copies Owned</label>
                <input type='number' min='1' value={no_of_copies} onChange={(e) => setCopiesOwned(e.target.value)} />
            </div>

            <input type='submit' value='Save Game' className='btn btn-block btn-submit' />
        </form>
    )
}

export default AddGame
