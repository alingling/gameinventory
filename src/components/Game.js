import { FaTimes} from 'react-icons/fa'
const Game = ({ game,onDelete}) => {
    return (
        <div className='event'>
            <h3>{game.game_name} <FaTimes style={{color:'red', cursor:'pointer'}} onClick={() => onDelete(game.id) } /></h3>
            <p>Players: <strong>{game.no_of_players}</strong></p>
            <p>Playing Time: <strong>{game.playing_time}</strong></p>
            <p>No of Copies Owned: <strong>{game.no_of_copies}</strong></p>
   
        </div>
    )
}

export default Game
