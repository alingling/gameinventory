import Game from './Game'
const Games = ({games,onDelete}) => {
    return (
        <>
           {games.map((game) => (
           <Game key={game.id} game={game} onDelete={onDelete}/>
           )
           )} 
        </>
    )
}

export default Games
